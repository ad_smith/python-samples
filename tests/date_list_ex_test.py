from date_list_ex import generate_dates
from datetime import date


def test_generate_dates():
    dates = list(generate_dates(date(2020, 2, 27), date(2020, 3, 2)))
    assert dates == ['2020-02-27', '2020-02-28', '2020-02-29', '2020-03-01', '2020-03-02']
