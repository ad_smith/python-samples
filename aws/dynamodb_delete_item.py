import boto3
from pprint import pprint


dynamodb = boto3.resource('dynamodb')
table = dynamodb.Table('sample-table')

response = table.delete_item(
    Key={
        'firstName': 'Andrew'
    },
)

pprint(response)
