import boto3
import random
from pprint import pprint


dynamodb = boto3.resource('dynamodb')
table = dynamodb.Table('sample-table')

response = table.put_item(
    Item={
        'firstName': 'Andrew',
        'status': 'in progress',
        'v1': random.randint(1, 1000000),
        'v2': random.randint(1, 1000000)
    }
)

pprint(response)
