"""
Demonstrates how to use pyautogui to auto-click the screen.
"""
import pyautogui


# Test this using https://www.openprocessing.org/sketch/186320/
# Interval is not affected by the pyautogui global pause
pyautogui.click(982, 561, clicks=50, interval=0.01)

# This is affected by the pyautogui global pause
pyautogui.PAUSE = 0.01
for _ in range(50):
    pyautogui.click(982, 561)
