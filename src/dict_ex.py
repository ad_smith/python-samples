import json


d = {
    'a': 23,
    'b': 11,
    'z': 55,
    'c': 31,
    'e': 44,
    'd': 51
}

print('Sorted by value:')
for key in sorted(d, key=d.get):
    print(key, d[key])

print('-' * 80)

print('Sorted by key:')
for key in sorted(d):
    print(key, d[key])

print('-' * 80)

print('Keys:', d.keys())
print('Values:', d.values())
print('Items:', d.items())
print('JSON:', json.dumps(d))
print('String:', str(d))
