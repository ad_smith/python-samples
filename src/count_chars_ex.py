"""
Prints the number of characters in a given string.
"""
from collections import Counter


def print_count(s):
    c = Counter(s)
    # Unsorted
    for k in c.keys():
        print(k, c[k])

    # Sorted by most frequent character
    print('-' * 80)
    for k in sorted(c, key=c.get, reverse=True):
        print(k, c[k])

    # Sorted by character
    print('-' * 80)
    for k in sorted(c):
        print(k, c[k])


print_count('hello world')
