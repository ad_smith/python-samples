import hashlib
from hashlib import sha256, md5


s = b'hello world'

h = sha256(s)
print('SHA-256:', h.hexdigest())

h = md5(s)
print('MD5:', h.hexdigest())

print(sorted(hashlib.algorithms_available))
