import os


def read_all_files(path):
    lines = []
    file_list = os.listdir(path)
    print(file_list)
    for file_name in file_list:
        with open(os.path.join(path, file_name), 'r') as f:
            lines.extend(f.readlines())
    return lines


lines = read_all_files('E:\\path')
print(len(lines))

