import logging
import sys


# log_format = '%(message)s'
log_format = '%(asctime)s %(name)s %(levelname)s [%(filename)s:%(lineno)d] %(message)s'
logging.basicConfig(stream=sys.stdout, level=logging.DEBUG, format=log_format)

logging.debug('debug')
logging.info('info')
logging.warning('warning')
logging.error('error')
logging.critical('critical')

logger = logging.getLogger('default_logger')
logger.debug('debug')

logging.info('info message with a number: %d', 123)
logging.info('info message with a string: %s', 'foo')

if logger.isEnabledFor(logging.DEBUG):
    logging.debug('debug level is enabled')

# Format a message with colors using ANSI escape sequences
# See https://stackoverflow.com/a/45924203/322785 for more examples
logging.info("\033[31m{}\033[0m".format("this is red"))
