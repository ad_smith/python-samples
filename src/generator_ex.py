"""
Demonstrates a generator function and how to use it.
"""


def generate_fib():
    f1 = 0
    f2 = 1
    yield f1
    while True:
        f1, f2 = f2, f1 + f2
        yield f1


gen = generate_fib()
for a in range(10):
    print(next(gen))

print([next(gen) for _ in range(10)])
