import re


s = 'This is my sentence'

# Search
res = re.search('^This.*sentence$', s)
print('Match:', res)
print(res.span())  # Prints (0, 19)
res = re.search('sentence', s)
print(res.span())  # Prints (11, 19)
res = re.search('abcdef', s)
print(res)  # Prints None
res = re.search('^This (.*) sentence$', s)
print(res.group(1))  # Prints is my

# Split
res = re.split(' ', s)
print('Split:', res)
print('Split:', s.split(' '))

# Match
# Match searches from the start of the string
# If you need to match at the beginning of the string, or to match the entire
# string, use match.  It is faster.  Otherwise, use search.
res = re.match('This is', s)
print(res)

# Substitutions
res = re.sub('is', 'zz', s)
print(res)
res = re.sub('This (.*) (.*) sentence', 'This \\2 \\1 sentence', s)
print(res)

# Group extraction
res = re.search('(.*) (.*) (.*) (.*)', s)
for i, group in enumerate(res.groups()):
    print(f'{group} at {res.span(i)}')
print(len(res.groups()))  # Prints 4
