import unittest.mock as mock
import sys

sys.modules['somepackage.foo'] = mock.Mock()

import module_a


def test_f1():
    assert module_a.f1() == 'f1'


def test_f2():
    assert module_a.f2() == 'f1-f2'


@mock.patch('module_a.f1')
def test_f2_f1_mocked(mock_f1):
    mock_f1.return_value = 'mock f1'
    assert module_a.f2() == 'mock f1-f2'
    mock_f1.assert_called_once()
