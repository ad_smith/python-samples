import os


def one_ping_only(hostname):
    """Pings the specified hostname once and returns True if successful or False
     otherwise.  Only works on Windows."""
    response = os.system("ping -n 1 " + hostname)
    return not response


print(one_ping_only("google.com"))

