import traceback as tb


def f1():
    print("hello world")
    f2()


def f2():
    print("inside f2")
    f3()


def f3():
    print("inside f3")
    raise Exception("error inside f3")


try:
    f1()
except Exception as e:
    print(tb.format_exc())
    # raise Exception(tb.format_exc()) from None  # Discard previous exception details
    raise Exception(tb.format_exc()) from e  # Include the traceback as part of the error message
