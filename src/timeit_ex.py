"""
Use timeit for timing snippets of Python code.  This works better than time_ns()
or other methods for very fast statements.
"""
from timeit import default_timer as timer
import time


start = timer()
time.sleep(1)
end = timer()

elapsed_ms = (end - start) * 1000
print(elapsed_ms, "ms elapsed")


start = time.time_ns()
time.sleep(1)
end = time.time_ns()
print(end - start, "ns elapsed")
print((end - start) / 1E6, "ms elapsed")
