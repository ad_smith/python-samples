import threading
import time
from queue import Queue

q = Queue()
num_threads = 8
n = 100


for i in range(n):
    s = f'Message {i}'
    q.put(s)


def handle_message():
    while not q.empty():
        msg = q.get()
        print(f'Thread {threading.get_ident()} retrieved message: {msg}')
        time.sleep(0.1)
        print(f'Thread {threading.get_ident()} finished processing message: {msg}')
        q.task_done()


for _ in range(num_threads):
    threading.Thread(target=handle_message).start()

q.join()
