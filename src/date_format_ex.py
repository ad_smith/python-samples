import datetime


# See https://strftime.org/ for a strftime reference
# Prints the date in local timezone
now = datetime.datetime.now()
print(now.strftime('%Y-%m-%d %H:%M:%S.%f'))

# Print the date using UTC
now = datetime.datetime.now(datetime.timezone.utc)
print(now.strftime('%Y-%m-%d %H:%M:%S.%f'))

# Truncate to milliseconds
print(now.strftime('%Y-%m-%d %H:%M:%S.%f')[:-3])

# Prints the day of the week ('Wednesday')
print(now.strftime("%A"))

# Prints the month ('May')
print(now.strftime("%B"))

# Locale appropriate date and time ('Mon Sep 30 07:06:05 2013')
print(now.strftime('%c'))

# Locale appropriate date ('05/13/20')
print(now.strftime('%x'))

# Locale appropriate time ('06:32:01')
print(now.strftime('%X'))

# Prints the date and time in ISO format
print(datetime.datetime.now().isoformat())
print(datetime.datetime.utcnow().isoformat())

# Set the separator for the format
print(datetime.datetime.utcnow().isoformat(sep=' '))

# Prints the date in ISO format
print(datetime.date.today().isoformat())

# Print the day of the week a particular date was
dow = datetime.date(2020, 3, 15).strftime('%A')
print(dow)

# Or do the same using isoformat:
dow = datetime.date.fromisoformat('2020-03-15').strftime('%A')
print(dow)

# Add a number of days to a date:
print((datetime.date(2020, 3, 15) + datetime.timedelta(days=35)).isoformat())
